const supertest = require('supertest')
const app = require("./server")

test("GET /", done => {
  supertest(app)
    .get("/")
    .expect(200, "Hello World!")
    .end(done)
})