FROM node:8.11

WORKDIR /usr/src/app

COPY package.json /usr/src/app/

RUN npm install --production

COPY . /usr/src/app

EXPOSE 8888
CMD [ "npm", "server.js" ]
